/* global Backbone, _, app */

app.Controller = Backbone.Router.extend({
    routes  : {
        "" : "main"
    },
    loadCommonElements  : function(){
        if(!this.headerView)
            this.headerView = new app.HeaderView().render();
        if(!this.mainView)
            this.mainView = new app.MainView();
        if(!this.footerView)
            this.footerView = new app.FooterView().render();
    },
    main: function() {
        if(!this.mainView)
            this.loadCommonElements();
        this.mainView = new app.MainView().render();
    }
});

$(document).ready(function() {     
     myroot = new app.Controller();
     Backbone.history.start();
});
