var app = app || {}, myroot;

app.Settings = {
    services_url    : "http://localhost:8080/roboworld/rest",
    addGetAllAliveRobots   : function() {
        return this.services_url + "/robo/robots";
    },
    getNewRobotUrl   : function(){
        return this.services_url + "/robo/robots/add";
    },
    addNewQueuePoint: function(){
        return this.services_url + "/task/add";
    },
    addAllQueuePoint: function(){
        return this.services_url + "/task/addall";
    },
    addAnyQueuePoint: function(){
        return this.services_url + "/task/addany";
    },
    getQueuePoints: function(){
        return this.services_url + "/task/getqueue?robotId=";
    },
    runGameStep : function(){
        return this.services_url + "/core/execute";
    },
    getLogs : function(){
        return this.services_url + "/robo/logs";
    },
    getRobotLogs : function(){
        return this.services_url + "/robo/logs/robotlogs";
    },
    setTask : function(){
        return this.services_url + "/robo/robots/taskQueue";
    },
};

app.displayTracker = 0;
  