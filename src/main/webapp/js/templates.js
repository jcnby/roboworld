app.tplHeader = '<div class = "row"><h2 style="font-family: constantia" align="center">ROBOWORLD</h2></div>';

app.tplFooter = '<nav class="navbar navbar-default navbar-fixed-bottom">\
<div class="container">\
    <h6 align="center">Copyright © 2016 ROBOWORLD. All Rights Reserved.</h6>\
</div>\
</div>';

app.tplMain = 'div class="row" id="mainRow">\
</div>';

app.tplMainRobotsPanel = '<div class="col-md-3" id="mainRobotContent">\
</div>';

app.tplOverTreckerPanel = '<div class="col-md-6" id="overActivityContent">\
</div>';
app.tplTreckerPanel = '<ul class="list-group" id="activityContent" style="max-height:800px;overflow: auto">\
</ul>';

app.tplMainTaskQueuePanel = '<div class="col-md-3" id="taskContent" style="min-width:300px">\
</div>';

app.tplRobotsPanel = '<ul class="list-group" style="margin-top: 10px;max-height:600px;overflow: auto">\
</ul>';

app.tplTaskQueuePanel = '<ul class="list-group" id="taskContainer" style="margin-top: 10px;max-height:600px;background-color:#cdcdc1;overflow: auto">\
</ul>';

app.tplRunButton = '<form class="form-inline" onsubmit="return false;">\
<button type="submit" class="btn btn-default" id="btn_runstep">make a step</button>\
</form>';

app.tplCurentTaskView1 ='<li class="list-group-item" id=<%=item.id%>>\
<span class="badge"><%= item.duration %>\
</span>\
<h6>kill yourself</h6>\
</li>';

app.tplCurentTaskView2 ='<li class="list-group-item" id=<%=item.id%>>\
<span class="badge"><%= item.duration %>\
</span>\
<h6>minning resourses</h6>\
</li>';

app.tplCurentTaskView3 ='<li class="list-group-item" id=<%=item.id%>>\
<span class="badge"><%= item.duration %>\
</span>\
<h6>explore the territory</h6>\
</li>';

app.tplCurentTaskView4 ='<li class="list-group-item" id=<%=item.id%>>\
<span class="badge"><%= item.duration %>\
</span>\
<h6>secure area</h6>\
</li>';

app.tplRoboView = '<li class="list-group-item" id=<%=item.id%>>\
<span class="badge">R<%= item.id %>T<%= item.type %>\
</span>\
<%= item.currentTask %>\
</li>';

app.tplActivityView = '<li class="list-group-item" id=<%=item.id%>>\
<span class="badge"><%=item.name%>\
</span>\
<%= item.activity %>\
</li>';

app.tplAddPanel = '<div class="container" style="max-width:150px;margin-left:10px">\
<h3>Robot task queue</h3>\
</div>';

app.tplBtnView = '<form class="form-inline" onsubmit="return false;">\
<button type="submit" class="btn btn-default" id="btn_newMiner">new Miner</button>\
<button type="submit" class="btn btn-default" id="btn_newExplorer">new Explore</button>\
<button type="submit" class="btn btn-default" id="btn_newNavy">new Navy</button>\
</form>';

app.tplEmptyRobotIdView = '<form class="form-inline" style="margin-left:10px">\
  <div class="form-group">\
    <button type="submit" class="btn btn-default" id="btn_all">all</button>\
    <button type="submit" class="btn btn-default" id="btn_any">any</button>\
  </div>\
  <div class="form-group">\
    <label class="sr-only">current Robot ID - </label>\
    <p class="form-control-static" id="currentRobotID"> current Robot <span class="badge">EMPTY</span>\</p>\
  </div>\
</form>';

app.tplCurrentRobotIdView = '<form class="form-inline" style="margin-left:10px;margin-top:10px" onsubmit="return false;">\
  <div class="form-group">\
    <button type="submit" class="btn btn-default" id="btn_all">all</button>\
    <button type="submit" class="btn btn-default" id="btn_any">any</button>\
  </div>\
  <div class="form-group">\
    <label class="sr-only">current Robot ID - </label>\
    <p class="form-control-static" id="currentRobotID"> current Robot <span class="badge">R<%= item.id %>T<%= item.type %></span>\</p>\
  </div>\
  <div class="form-group">\
    <select class="form-control" id ="taskListForCurrentRobot">\
        <option value="1">kill yourself</option>\
        <option value="2">minning resourses</option>\
        <option value="3">explore the territory</option>\
        <option value="4">secure area</option>\
    </select>\
    <button type="submit" class="btn btn-default" id="btn_apply">apply</button>\
    <button type="submit" class="btn btn-default" id="btn_unset">x</button>\
  </div>\
</form>';

app.tplAllCurrentRobotIdView = '<form class="form-inline" style="margin-left:10px;margin-top:10px" onsubmit="return false;">\
  <div class="form-group">\
    <button type="submit" class="btn btn-primary" id="btn_all">all</button>\
    <button type="submit" class="btn btn-default" id="btn_any">any</button>\
  </div>\
  <div class="form-group">\
    <label class="sr-only">current Robot ID - </label>\
    <p class="form-control-static" id="currentRobotID"> current Robot <span class="badge">All</span>\</p>\
  </div>\
  <div class="form-group">\
    <select class="form-control" id ="taskListForCurrentRobot">\
        <option value="1">kill yourself</option>\
        <option value="2">minning resourses</option>\
        <option value="3">explore the territory</option>\
        <option value="4">secure area</option>\
    </select>\
    <button type="submit" class="btn btn-default" id="btn_apply">apply</button>\
    <button type="submit" class="btn btn-default" id="btn_unset">x</button>\
  </div>\
</form>';

app.tplAnyCurrentRobotIdView = '<form class="form-inline" style="margin-left:10px;margin-top:10px" onsubmit="return false;">\
  <div class="form-group">\
    <button type="submit" class="btn btn-default" id="btn_all">all</button>\
    <button type="submit" class="btn btn-primary" id="btn_any">any</button>\
  </div>\
  <div class="form-group">\
    <label class="sr-only">current Robot ID - </label>\
    <p class="form-control-static" id="currentRobotID"> current Robot <span class="badge">any</span>\</p>\
  </div>\
  <div class="form-group">\
    <select class="form-control" id ="taskListForCurrentRobot">\
        <option value="1">kill yourself</option>\
        <option value="2">minning resourses</option>\
        <option value="3">explore the territory</option>\
        <option value="4">secure area</option>\
    </select>\
    <button type="submit" class="btn btn-default" id="btn_apply">apply</button>\
    <button type="submit" class="btn btn-default" id="btn_unset">x</button>\
  </div>\
</form>';