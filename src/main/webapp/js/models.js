app.RobotModel = Backbone.Model.extend({
    urlRoot: function(){ return app.Settings.getNewRobotUrl(); },
    defaults : {
        id              : null,
	currentTask     : '',
        alive       	: false,
        type    	: 0
    }
});

app.RobotsCollection = Backbone.Collection.extend({
    model: app.RobotModel,
    url : function(){ 
        return app.Settings.addGetAllAliveRobots(); 
    }
});

app.ActivityModel = Backbone.Model.extend({
    defaults : {
        id              : 0,
        name            : '',
        activity        : ''
    }
});

app.ActivityCollection = Backbone.Collection.extend({
    model: app.ActivityModel,
    url : function(){ 
        return app.Settings.getLogs(); 
    }
});

app.TaskModel = Backbone.Model.extend({
    defaults : {
        id              : 0,
        type            : 0,
        name            : '',
        target          : 0
    }
});

app.TaskQueue = Backbone.Collection.extend({
    urlRoot: function(){ 
        return app.Settings.setTask(); 
    },
    model: app.TaskModel
});

app.TaskListQueue = new app.TaskQueue();

app.CurrentRobotModel = Backbone.Model.extend({
    urlRoot: function(){ 
        return app.Settings.changeCurrentRobot(); 
    },
    defaults : {
        id              : -1,
        currentTask     : '',
        alive       	: false,
        type    	: 0
    }
});

app.TaskEntry = Backbone.Model.extend({
    defaults : {
        id	: null,
	type 	: 0,
	name	: ''
    }
});

app.CurrentRobot = new app.CurrentRobotModel();

app.QueuePoint = Backbone.Model.extend({
    urlRoot : function(){ 
        return app.Settings.addNewQueuePoint(); 
    },
    defaults : {
        id          : null,
        roboid      : 0,
        taskid      : 0,
        duration    : 0,
        executed    : false,
        currenttask : false
    }
});

app.QueuePointCollection = Backbone.Collection.extend({
    url: function(){ 
        return app.Settings.getQueuePoints(); 
    },
    model: app.QueuePoint
});