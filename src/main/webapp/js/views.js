app.BaseView = Backbone.View.extend({
    initialize    : function() {
        this.setElement(this.template());
    },
    empty   : function(){
        this.$el.empty();
    },
    append  : function(element){
        this.$el.append(element);
    },
    loadView: function(view){
        this.empty();
        this.append(view.el);
    },
    appendView: function(view){
        this.append(view.el);
    },
    render  : function() {
        return this;
    }
});

// HEADER
app.HeaderView = app.BaseView.extend({
    el : $('#header'),
    template        : _.template(app.tplHeader),
    initialize      : function() {
    },
    render: function() {
        this.empty();
        this.append(this.template());
        return this;
    }
});

// MAIN
app.MainView = app.BaseView.extend({
    el      : $('#main'),
    template        : _.template(app.tplMain),
    initialize      : function() {
    },
	render          : function(){
        this.empty();

        this.mainRobotPanel = new app.MainRobotView().render();
        this.appendView(this.mainRobotPanel);

        this.overTreckerPanel = new app.OverTreckerView().render();
        this.appendView(this.overTreckerPanel);
		
		this.mainTaskQueuePanel = new app.MainTaskQueueView().render();
        this.appendView(this.mainTaskQueuePanel);
		
    }
});

app.OverTreckerView = app.BaseView.extend({
    template        : _.template(app.tplOverTreckerPanel),
    render          : function(){
        this.empty();

        this.runButtonPanel = new app.RunButtonView().render();
        this.appendView(this.runButtonPanel);

        this.treckerPanel = new app.TreckerView().render();
        this.appendView(this.treckerPanel);
        
        return this;
    }
});

app.RunButtonView = app.BaseView.extend({
    template        : _.template(app.tplRunButton),
    events : {
        "click .btn" : "runGameStep"
    },
    runGameStep : function(event){
        Backbone.ajax({
            dataType: "json",
            url: app.Settings.runGameStep(),
            data: "",
            success: function (val) {
            },
            error : function (val) {
            }
        });
    },
    render          : function(){
        this.empty();
        this.append(this.template);
        return this;
    }
});

app.MainRobotView = app.BaseView.extend({
    template        : _.template(app.tplMainRobotsPanel),
	render          : function(){
        this.empty();
        
        this.createBtnPanel = new app.CreateBtnView().render();
        this.appendView(this.createBtnPanel);

        this.labelRobotIdPanel = new app.LabelRobotView().render();
        this.appendView(this.labelRobotIdPanel);

        this.robotsPanel = new app.RobotsView().render();
        this.appendView(this.robotsPanel);  
        
        return this;
    }
});

app.RobotsView = app.BaseView.extend({
    template        : _.template(app.tplRobotsPanel),
    collection      : new app.RobotsCollection(),
	events: {
        "click .list-group-item" : "getRobotId"
    },
    getRobotId: function (event) {
		app.CurrentRobot = this.collection.get(+event.currentTarget.id).clone();
        app.displayTracker=1;
		Backbone.trigger('select-current-robot');
        Backbone.trigger('select-current-robot-activity');
    },
    initialize: function() {    
        this.setElement(this.template());
        this.listenTo(this.collection, 'reset', this.render);
        var that = this;
        this.draw();
		app.timerId = setInterval(_.bind(this.draw, this), 1000);
    },
	draw    : function() {  // вытягивает данные с сервера
        this.collection.fetch({reset: true});
    },
    add  : function(model){
        this.appendView(new app.RobotView({model: model}).render());
    },
    render      : function(){
        var that = this;
        this.empty();
        this.collection.each(function(model){ that.add(model); });
        return this;
    }
});


app.CreateBtnView = app.BaseView.extend({
    template        : _.template(app.tplBtnView),
	events: {
        "click .btn" : "newRobo"
    },
    newRobo: function (event) {
	var newRbobor = new app.RobotModel();
	if(event.currentTarget.id == "btn_newMiner"){
            newRbobor.set("type",1);
	}else if(event.currentTarget.id == "btn_newExplorer"){
            newRbobor.set("type",2);
	}else{
            newRbobor.set("type",3);
	}
	newRbobor.set("currentTask","free");
	newRbobor.set("alive",true);
	newRbobor.save();	
    },
    render          : function(){
        this.empty();
        this.append(this.template);
        return this;
    }
});

app.LabelRobotView = app.BaseView.extend({
    template        : _.template(app.tplAnyCurrentRobotIdView),
	model			: app.CurrentRobot,
	events: {
        "click .btn" : "changeCurrentRobot"
    },
    changeCurrentRobot: function (event) {
        if(event.currentTarget.id == "btn_apply"){
            // id
            var newAueuePoint = new app.QueuePoint();
            if(app.CurrentRobot.id == -1){
                newAueuePoint.url = app.Settings.addAnyQueuePoint(); 
            }else if(app.CurrentRobot.id == -2){
                newAueuePoint.url = app.Settings.addAllQueuePoint();
            }else{
                newAueuePoint.url = app.Settings.addNewQueuePoint();
            }
            newAueuePoint.set("roboid",app.CurrentRobot.id); // -1 any // -2 all
            newAueuePoint.set("taskid",+$("#taskListForCurrentRobot").find("option:selected").attr("value")); // id task!!!!
            newAueuePoint.save();
        }else if(event.currentTarget.id == "btn_all"){
            app.CurrentRobot.id = -2;
            Backbone.trigger('select-current-robot-activity');
            this.redraw();
        }else if(event.currentTarget.id == "btn_any"){
            app.CurrentRobot.id = -1;
            Backbone.trigger('select-current-robot-activity');
            this.redraw();
        }else{
            app.displayTracker=0;
            Backbone.trigger('select-current-robot-activity');
        } 
    },
    initialize    	: function() {
        Backbone.on('select-current-robot', this.redraw, this);
    },
    redraw          : function(){
        if(app.CurrentRobot.id > 0){
            this.template = _.template(app.tplCurrentRobotIdView);
	}else if(app.CurrentRobot.id == -1){
            this.template = _.template(app.tplAnyCurrentRobotIdView);
	}else{
            this.template = _.template(app.tplAllCurrentRobotIdView);
        }
        this.render();
    },
    render          : function(){
        this.empty();
        this.append(this.template({item: app.CurrentRobot.toJSON()}));
        return this;
    }
});

app.RobotView = app.BaseView.extend({
    template    : _.template(app.tplRoboView),
    initialize    : function() {
        this.setElement(this.template({item: this.model.toJSON()}));
    },
    render      : function(){
        return this;
    }
});
// main view center
app.TreckerView = app.BaseView.extend({
    template        : _.template(app.tplTreckerPanel),
    collection      : new app.ActivityCollection(),
    initialize  : function() {    
        this.setElement(this.template());
        this.listenTo(this.collection, 'reset', this.render);
        Backbone.on('select-current-robot-activity', this.redraw, this);
        var that = this;
        this.draw();
        app.timerTreckView = setInterval(_.bind(this.draw, this), 1000);
    },
    redraw    : function() {  // вытягивает данные с сервера
        if(app.displayTracker==0){
            this.collection.url = app.Settings.getLogs();
        }else{
            this.collection.url = app.Settings.getRobotLogs() + "?name=R"+app.CurrentRobot.id+"T"+app.CurrentRobot.attributes.type;
        }
        this.collection.fetch({reset: true});
    },
    draw    : function() {  // вытягивает данные с сервера
        this.collection.fetch({reset: true});
    },
    add         : function(model){
        this.appendView(new app.ActivityView({model: model}).render());
    },
    render      : function(){
        var that = this;
        this.empty();
        this.collection.each(function(model){ that.add(model); });
        return this;
    }
});

app.ActivityView = app.BaseView.extend({
    template    : _.template(app.tplActivityView),
    initialize    : function() {
        this.setElement(this.template({item: this.model.toJSON()}));
    },
    render      : function(){
        return this;
    }
});

app.MainTaskQueueView = app.BaseView.extend({
    template        : _.template(app.tplMainTaskQueuePanel),
    initialize: function() {
        this.setElement(this.template());
    },
    render          : function(){
        this.empty();
        this.addPanel = new app.AddPanelView().render();
        this.appendView(this.addPanel);
        this.taskPanel = new app.TaskQueueView().render();
        this.appendView(this.taskPanel);  
        return this;
    }
});


app.TaskQueueView = app.BaseView.extend({
    template        : _.template(app.tplTaskQueuePanel),
    collection      : new app.QueuePointCollection(),
    initialize  : function() {
        this.setElement(this.template());
        this.collection.url = app.Settings.getQueuePoints() + app.CurrentRobot.id;
        this.listenTo(this.collection, 'reset', this.render);
        Backbone.on('select-current-robot-activity', this.redraw, this);
        var that = this;
        this.draw();
        app.timerQueueView = setInterval(_.bind(this.draw, this), 1000);
    },
    redraw    : function() {  // вытягивает данные с сервера
        this.collection.url = app.Settings.getQueuePoints() + app.CurrentRobot.id;
    },
    draw    : function() {  // вытягивает данные с сервера
        this.collection.fetch({reset: true});
    },
    add         : function(model){
        this.appendView(new app.CurrentTaskQueView({model: model}).render());
    },
    render      : function(){
        var that = this;
        this.empty();
        this.collection.each(function(model){ that.add(model); });
        return this;
    }
});

app.AddPanelView = app.BaseView.extend({
    template        : _.template(app.tplAddPanel),
    render          : function(){
        this.empty();
        this.append(this.template);
        return this;
    }
});

app.CurrentTaskQueView = app.BaseView.extend({
    template    : _.template(app.tplCurentTaskView1),
    initialize    : function() {
        if(this.model.get("taskid")==2){
            this.template = _.template(app.tplCurentTaskView2);
        }else if(this.model.get("taskid")==3){
            this.template = _.template(app.tplCurentTaskView3);
        }else if(this.model.get("taskid")==4){
            this.template = _.template(app.tplCurentTaskView4);
        };
        this.setElement(this.template({item: this.model.toJSON()}));
    },
    render      : function(){
        return this;
    }
});

//FOOTER
app.FooterView = app.BaseView.extend({
    el              : $('#footer'),
    template        : _.template(app.tplFooter),
    initialize      : function() {
        this.empty();
        this.append(this.template());
    }
});