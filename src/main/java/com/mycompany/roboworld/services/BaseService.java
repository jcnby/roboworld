package com.mycompany.roboworld.services;

import com.mycompany.roboworld.datamgmt.DataService;
import com.mycompany.roboworld.entities.Machine;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.TimeZone;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

public class BaseService extends DataService{
    
    public static final Charset CHARSET = Charset.forName("UTF-8");
    
    public BaseService(){
        super();
    }

    @QueryParam("name")
    public String name;
    
    @QueryParam("robotId")
    public int robotId;
	
    @Context
    public UriInfo uriInfo;
	
    @GET
    @Path("/info")
    @Produces(MediaType.TEXT_PLAIN)
    public String systemInfo() {
        String output = 
            "Name: " + this.getClass().getSimpleName()+"\n\n"+
            "URL: " + uriInfo.getAbsolutePath() + "\n\n"+
            "Memory usage: \n" 
            + "TOTAL\t: "	+ Runtime.getRuntime().totalMemory() / 1000000 + " Mb\n" 
            + "FREE\t: "	+ Runtime.getRuntime().freeMemory() / 1000000 + " Mb\n" 
            + "MAX\t: "	+ Runtime.getRuntime().maxMemory() / 1000000 + " Mb\n\n"
            +"TimeZone: "+ TimeZone.getDefault().getDisplayName()+ "; "+TimeZone.getDefault().getID()+"\n";
	return output;
    }
    
    
    // SELECT
    public Response buildGETResponse(Object entity){
        if(entity == null) entity = "{\"ERROR\":\"entity is null\"}";
	try{
            return Response
            .status(200)
            .entity(entity)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET")
            .allow("OPTIONS")
            .build();
	} finally {
            closeSqlSession();
	}
    }
	
    // INSERT
    public Response buildPOSTResponse(Object entity){
        if(entity == null) entity = "{\"ERROR\":\"entity is null\"}";
        try{			
            return Response
            .status(201)
            .entity(entity)
            .header("Access-Control-Allow0-Origin", "*")
            .header("Access-Control-Allow-Methods", "POST")
            .allow("OPTIONS")
            .build();
        } finally {
            commit();
            closeSqlSession();
        }
    }
    
    // UPDATE
    public Response buildPUTResponse(Object entity){
        if(entity == null) entity = "{\"ERROR\":\"entity is null\"}";
        try{			
            return Response
            .status(200)
            .entity(entity)
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "PUT")
            .allow("OPTIONS")
            .build();
        } finally {
            commit();
            closeSqlSession();
        }
    }
    
    public static Comparator getComparator(int type){
        final int typeForComporator = type; 
        Comparator<Machine> comparator = new Comparator<Machine>() {
            @Override
            public int compare(Machine robo1, Machine robo2) {
                if(typeForComporator!=0){
                    if( robo1.getType() == typeForComporator && robo2.getType() != typeForComporator){
                        return -1;
                        }
                    if( robo1.getType() != typeForComporator && robo2.getType() == typeForComporator){
                        return 1;
                    }
                    if( robo1.getType() == typeForComporator && robo2.getType() == typeForComporator){   
                        if(robo1.getId() > robo2.getId()){
                            return 1;
                        }
                        if(robo1.getId() < robo2.getId()){
                            return -1;
                        }
                    }
                }else{
                    if(robo1.getId() > robo2.getId()){
                        return 1;
                    }
                    if(robo1.getId() < robo2.getId()){
                        return -1;
                    }
                }
                return 0;
            }
        };
        return comparator;
    }
}

