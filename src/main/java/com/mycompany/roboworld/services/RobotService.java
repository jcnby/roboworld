package com.mycompany.roboworld.services;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.util.ExplorerMaker;
import com.mycompany.roboworld.util.MilitaryMaker;
import com.mycompany.roboworld.util.MinerMaker;
import com.mycompany.roboworld.util.Robot;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("robo")
public class RobotService extends BaseService{
    
	// example http://localhost:8080/roboworld/rest/robo/info
    @GET
    @Path("/robots")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRobots() {
        return buildGETResponse(getMapper().getAllAlivesMachine());
    }
    
    @POST
    @Path("/robots/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response newMachine(Machine machine) {    
        Robot newRobot;
        if(machine.getType()==1){
            newRobot = new MinerMaker().createRobot();
        }else if(machine.getType()==2){
            newRobot = new ExplorerMaker().createRobot();
        }else if(machine.getType()==3){
            newRobot = new MilitaryMaker().createRobot();
        }        
        return buildPOSTResponse(machine);
    }
    
    @GET
    @Path("/logs")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLogs() {
        return buildGETResponse(getMapper().getAllNote());
    }
    
    @GET
    @Path("/logs/robotlogs")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLogsByName() {
        return buildGETResponse(getMapper().getNoteByName(name));
    }
    
}
