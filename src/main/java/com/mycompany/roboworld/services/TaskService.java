package com.mycompany.roboworld.services;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.QueuePoint;
import com.mycompany.roboworld.entities.Task;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("task")
public class TaskService extends BaseService{
    
	// example http://localhost:8080/roboworld/rest/task/info
    @GET
    @Path("/getqueue")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRobotTask() {
        return buildGETResponse(getMapper().getQueue(robotId));
    }
    
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response newRobotTask(QueuePoint point) {  
        try {
            Task task = getMapper().getTaskById(point.getTaskid());
            Machine machine = getMapper().getMachineById(point.getRoboid());
            if(task.getType() != 0 && task.getType() != machine.getType()){
                return buildGETResponse("Can't set this task to this robo");
            }
            point.setDuration(task.getDuration());
            List<QueuePoint> taskList = getMapper().getQueue(machine.getId());
            if(taskList.size()>0){
                point.setCurrenttask(Boolean.FALSE);
                point.setExecuted(Boolean.FALSE);
                getMapper().addNewQueue(point);
            }else{
                point.setCurrenttask(Boolean.TRUE);
                point.setExecuted(point.getTaskid()==1?Boolean.TRUE:Boolean.FALSE);
                getMapper().addNewQueue(point);
                machine.setCurrentTask(task.getName());
                machine.saveRobot();
                machine.executeTask();
            }
            
        }catch (Exception e){
            return buildGETResponse("error");
        }
        return buildPOSTResponse(point);
    }
    
    @POST
    @Path("/addany")
    @Produces(MediaType.APPLICATION_JSON)
    public Response newAnyRobotTask(QueuePoint point) {
        try {
            Task task = getMapper().getTaskById(point.getTaskid());
            point.setDuration(task.getDuration());
        } catch (Exception e) {
            return buildGETResponse("error");
        }
        return buildPOSTResponse(getMapper().addNewQueue(point));
    }
    
    @POST
    @Path("/addall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response newAllRobotTask(QueuePoint point) {
        try {
            Task task = getMapper().getTaskById(point.getTaskid());
            point.setDuration(task.getDuration());
        } catch (Exception e) {
            return buildGETResponse("error");
        }
        return buildPOSTResponse(getMapper().addNewQueue(point));
    }
}
