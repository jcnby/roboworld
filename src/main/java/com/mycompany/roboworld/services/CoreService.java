package com.mycompany.roboworld.services;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.QueuePoint;
import com.mycompany.roboworld.entities.Task;
import com.mycompany.roboworld.util.ExplorerMaker;
import com.mycompany.roboworld.util.MilitaryMaker;
import com.mycompany.roboworld.util.MinerMaker;
import com.mycompany.roboworld.util.Robot;
import com.mycompany.roboworld.util.RobotMaker;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("core")
public class CoreService extends BaseService{
	
	// example http://localhost:8080/roboworld/rest/core/info
    @GET
    @Path("/execute")
    @Produces(MediaType.APPLICATION_JSON)
    public Response rotateGame() {
        List<QueuePoint> queueTask = new ArrayList<QueuePoint>();
        List<Machine> freeMashine = new ArrayList<Machine>();
        List<Machine> allMashine = new ArrayList<Machine>();
        try {
            queueTask = getMapper().getAllUnsetQueuePonit();
            freeMashine = getMapper().getAllFreeMashine();
            allMashine = getMapper().getAllAlivesMachine();
            for(QueuePoint qPoint : queueTask){
                //any
                Task task = getMapper().getTaskById(qPoint.getTaskid());
                qPoint.setDuration(task.getDuration());
                if(qPoint.getRoboid() == -1){
                    Machine currentRobo = getExecutor(allMashine,freeMashine,task);
                    boolean freeRobo = currentRobo.getCurrentTask().equals("empty");
                    insertQueue(new QueuePoint(currentRobo.getId(), qPoint.getTaskid(), qPoint.getDuration(), freeRobo && qPoint.getTaskid()==1?Boolean.TRUE:Boolean.FALSE,freeRobo));
                    if(freeRobo){
                        updateMachine(currentRobo,task.getName());
                    }
                }else{
                    // all // -2
                    for(Machine machine: allMashine){
                        if(task.getType() == 0 || machine.getType()==task.getType()){
                            Boolean isFree = isCurrentTask(freeMashine,machine);
                            insertQueue(new QueuePoint(machine.getId(), qPoint.getTaskid(), qPoint.getDuration(), isFree && qPoint.getTaskid()==1?Boolean.TRUE:Boolean.FALSE,isFree));
                            if(isFree){
                                updateMachine(machine,task.getName());   
                            }
                        }    
                    }
                }
                qPoint.setExecuted(Boolean.TRUE);
                updateQueue(qPoint);
            }
        } catch (Exception e) {
            return buildGETResponse("error - set tasks");
        }finally{
            closeSqlSession();
        }
        // set all -1 duration
        try {
            getMapper().updateAllQueue();
        } catch (Exception e) {
            return buildGETResponse("error - updateAllQueue");
        }finally{
            commit();
        }
        // get all ended task!!!
        List<QueuePoint> tasks = new ArrayList<QueuePoint>();
        try {
            tasks = getMapper().getEndedQueuePonit(); 
            for(QueuePoint  task: tasks){
                Machine currentRobot =  getMapper().getMachineById(task.getRoboid());
                if(!currentRobot.getAlive()){
                    continue;
                }
                task.setCurrenttask(Boolean.FALSE);
                task.setExecuted(Boolean.TRUE);
                updateQueue(task);
                //get next !!!
                List<QueuePoint> nextTasks = getMapper().getQueue(currentRobot.getId());
                if(nextTasks.size() > 0){
                    int taskId = nextTasks.get(0).getTaskid();
                    Task newTask = getMapper().getTaskById(taskId);
                    QueuePoint nextTask = nextTasks.get(0);
                    nextTask.setCurrenttask(Boolean.TRUE);
                    nextTask.setExecuted(nextTask.getTaskid()==1?Boolean.TRUE:Boolean.FALSE);
                    updateQueue(nextTask);
                    updateMachine(currentRobot,newTask.getName());
                }else{
                    updateMachine(currentRobot,"empty");
                }     
            }
        } catch (Exception e) {
            return buildGETResponse("error - execute task");
        }finally{
            closeSqlSession();
        }
        return buildGETResponse("successful!");
    }

    private Machine getExecutor(List<Machine> allMashine, List<Machine> freeMashineList,Task task){
        Machine currentExecutor = new Machine();
        if(freeMashineList.size()==0){
            Robot currentRobo = getCreator(task.getType()).createRobot();
            allMashine.add(currentRobo.getMachine());
            currentExecutor = currentRobo.getMachine();   
        }else{
            Queue<Machine> freeMashineQueue = new PriorityQueue<Machine>(freeMashineList.size(),getComparator(task.getType()));    
            freeMashineQueue.addAll(freeMashineList);
            Machine robo = freeMashineQueue.peek();
            if(task.getType() == 0 || robo.getType()==task.getType()){
                robo = freeMashineQueue.poll();
                freeMashineList.remove(robo);
                currentExecutor = robo;        
            }else{
                // new robo!
                Robot currentRobo = getCreator(task.getType()).createRobot();
                allMashine.add(currentRobo.getMachine());
                currentExecutor = currentRobo.getMachine();
            }
        }
        return currentExecutor;
    }
    
    private RobotMaker getCreator(int type){
        RobotMaker creator = new MinerMaker();
        if(type==0 || type==1){
            creator = new MinerMaker();   
        }else if(type==2){
            creator = new ExplorerMaker();
        }else if(type==3){
            creator = new MilitaryMaker();
        }
        return creator;
    }
    
    private void insertQueue(QueuePoint newQueue){
        getMapper().addNewQueue(newQueue);
        commit();
    }
    
    private void updateQueue(QueuePoint currentQueue){
        getMapper().updateQueue(currentQueue);
        commit();
    }
    
    private void updateMachine(Machine currentRobot,String nameTask){
        currentRobot.setCurrentTask(nameTask);
        currentRobot.saveRobot();
        currentRobot.executeTask();
    }
    
     private Boolean isCurrentTask(List<Machine> currentList, Machine currentMachine){
        Boolean result = false;
        for(Machine cMachine : currentList){
            if(cMachine.getId()==currentMachine.getId()){
                result = true;
                break;
            }
        }
        return result;
    }
    
}
