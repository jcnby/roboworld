package com.mycompany.roboworld.entities;

public class QueuePoint {
    
    private int id;
    private int roboid;
    private int taskid;
    private int duration;
    private Boolean executed;
    private Boolean currenttask;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoboid() {
        return roboid;
    }

    public void setRoboid(int roboid) {
        this.roboid = roboid;
    }

    public int getTaskid() {
        return taskid;
    }

    public void setTaskid(int taskid) {
        this.taskid = taskid;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getExecuted() {
        return executed;
    }

    public void setExecuted(Boolean executed) {
        this.executed = executed;
    }

    public Boolean getCurrenttask() {
        return currenttask;
    }

    public void setCurrenttask(Boolean currenttask) {
        this.currenttask = currenttask;
    }
     
    public QueuePoint() {
    }

    public QueuePoint(int roboid, int taskid, int duration, Boolean executed, Boolean currenttask) {
        this.roboid = roboid;
        this.taskid = taskid;
        this.duration = duration;
        this.executed = executed;
        this.currenttask = currenttask;
    }
    
}
