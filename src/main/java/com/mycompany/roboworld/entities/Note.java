package com.mycompany.roboworld.entities;

public class Note {
    
    private int id;
    private String name;
    private String activity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
    
    public Note(){
    }
    
    public Note(String name, String activity){
        this.name = name;
        this.activity = activity;
    }
    
}
