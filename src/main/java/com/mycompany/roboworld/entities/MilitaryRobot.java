package com.mycompany.roboworld.entities;

import com.mycompany.roboworld.util.Robot;

public class MilitaryRobot extends Machine implements Robot{

    public MilitaryRobot(){
        this.alive = true;
        this.type = 3;
        this.currentTask = "empty";
        if(this.saveRobot()==1){
            this.saveRobotActivity("robot created!");
        }
    }
    
    public MilitaryRobot(Machine machine){
        this.id = machine.getId();
        this.currentTask = machine.getCurrentTask();
        this.alive = machine.getAlive();
        this.type = machine.getType();
    }
    
    public void setTask(String task){
        this.currentTask = task;
        executeTask();
    }

    public Boolean getRobotAlive() {
        return this.alive;
    }
    
    public Machine getMachine(){
        return new Machine(this.id,this.currentTask, this.alive, this.type);
    }
    
}
