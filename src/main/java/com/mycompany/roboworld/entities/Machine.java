package com.mycompany.roboworld.entities;

import com.mycompany.roboworld.datamgmt.RoboLog;
import com.mycompany.roboworld.util.Robot;

public class Machine{
    
    protected int id;
    protected String currentTask;
    protected Boolean alive;
    protected int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(String currentTask) {
        this.currentTask = currentTask;
    }

    public Boolean getAlive() {
        return alive;
    }

    public void setAlive(Boolean alive) {
        this.alive = alive;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    public Machine(){        
    }

    public Machine(int id, String currentTask, Boolean alive, int type){
        this.id = id;
        this.currentTask = currentTask;
        this.alive = alive;
        this.type = type;
    }    
    
    public void executeTask() {
        if(this.currentTask.equals("kill yourself")){
            this.alive = false;
            this.saveRobotActivity("robot destroyed!");
        }else{
            this.saveRobotActivity("current task is : " + this.getCurrentTask());
        }
        this.saveRobot();
    }
    
    public int saveRobot(){
        
        int result;
        
        result = new RoboLog().save(this);
        
        return result;
        
    }
    
    public int saveRobotActivity(String activity){
        
        int result;
        
        result = new RoboLog().saveActivity(this, activity);
        
        return result;
        
    }
    
}
