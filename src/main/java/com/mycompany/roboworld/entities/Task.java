package com.mycompany.roboworld.entities;

public class Task {

	private int id;
    private String name;
    private int duration;
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Task(String name, int duration, int type) {
        this.name = name;
        this.duration = duration;
        this.type = type;
    }
    
    public Task() {
    }
    
}
