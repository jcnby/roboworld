package com.mycompany.roboworld.datamgmt;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.Note;
import com.mycompany.roboworld.entities.QueuePoint;
import com.mycompany.roboworld.entities.Task;
import java.util.List;
import org.apache.ibatis.annotations.*;

public interface Mapper {   
    
    // **************************** create Data Base ************************ //
    
    // try to create DB
    @Insert("create database robotworld;")
    public int createNewBase();
    
    // try to drop table
    @Insert("DROP TABLE IF EXISTS robots;")
    public int dropTable();
    
    // try to create table
    @Insert("CREATE TABLE IF NOT EXISTS robots ( " 
        + "id INT NOT NULL AUTO_INCREMENT, "
        + "currentTask TEXT NULL DEFAULT NULL, "
        + "alive BIT NULL DEFAULT b'0', "
        + "type INT NULL DEFAULT NULL, "
        + " PRIMARY KEY (id));")
    public int createRobotTable();
    
    // try to drop table
    @Insert("DROP TABLE IF EXISTS logs;")
    public int dropLogTable();
    
    // try to create table Log
    @Insert("CREATE TABLE IF NOT EXISTS logs ( " 
        + "id INT NOT NULL AUTO_INCREMENT, "
        + "name TEXT NULL DEFAULT NULL, "
        + "activity TEXT NULL DEFAULT NULL, "
        + " PRIMARY KEY (id));")
    public int createLogTable();
    
    // try to drop table
    @Insert("DROP TABLE IF EXISTS tasks;")
    public int dropTaskTable();
    
    // try to create table
    @Insert("CREATE TABLE IF NOT EXISTS tasks ( " 
        + "id INT NOT NULL AUTO_INCREMENT, "
        + "name TEXT NULL DEFAULT NULL, "
        + "duration INT NULL DEFAULT NULL, "
        + "type INT NULL DEFAULT NULL, "
        + " PRIMARY KEY (id));")
    public int createTaskTable();
    
    // try to drop table
    @Insert("DROP TABLE IF EXISTS queue;")
    public int dropQueueTable();
    
    // try to create table
    @Insert("CREATE TABLE IF NOT EXISTS queue ( " 
        + "id INT NOT NULL AUTO_INCREMENT, "
        + "roboid INT NOT NULL DEFAULT 0, "
        + "taskid INT NOT NULL DEFAULT NULL, "
        + "duration INT NOT NULL DEFAULT 0, "
        + "executed BIT NULL DEFAULT b'0', "
        + "currenttask BIT NULL DEFAULT b'0', "
        + " PRIMARY KEY (id));")
    public int createQueueTable();
    
    // **************************** tasks            ************************ //
    
    @Insert("INSERT INTO tasks (name, duration, type)"
        + " VALUES ("
	+ "#{name},"
	+ "#{duration},"
        + "#{type})")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache=true) 
    public int addNewTask(Task task);
    
    @Update("UPDATE tasks SET "
        + "name = #{name}, "
        + "duration = #{duration}, "
        + "type = #{type} "
        + "WHERE id = #{id}")
    @Options(flushCache = true,useCache=false)
    public int updateTask(Task task);
    
    @Select("SELECT * FROM tasks "
        + " WHERE tasks.id = #{id}")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "type", column = "type")
	})
    public Task getTaskById(@Param("id") int id);
    
    @Select("SELECT * FROM tasks")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "type", column = "type")
	})
    public List<Task> getTasks();
    
    // **************************** queue            ************************ //   
    @Insert("INSERT INTO queue (roboid, taskid, duration, executed, currenttask)"
        + " VALUES ("
	+ "#{roboid},"
	+ "#{taskid},"
        + "#{duration},"
        + "#{executed},"
        + "#{currenttask})")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache=true) 
    public int addNewQueue(QueuePoint point);
    
    @Update("UPDATE queue SET "
        + "roboid = #{roboid}, "
        + "taskid = #{taskid}, "
        + "duration = #{duration}, "
        + "executed = #{executed}, "
        + "currenttask = #{currenttask} "
        + "WHERE id = #{id}")
    @Options(flushCache = true,useCache=false)
    public int updateQueue(QueuePoint point);
    
    @Select("SELECT * FROM queue "
        + " WHERE queue.id = #{id}")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "roboid", column = "roboid"),
        @Result(property = "taskid", column = "taskid"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "executed", column = "executed"),
        @Result(property = "currenttask", column = "currenttask")
	})
    public QueuePoint getQueuePointById(@Param("id") int id);
    
    @Select("SELECT * FROM queue "
        + " WHERE roboid = #{roboid} AND executed = 0")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "roboid", column = "roboid"),
        @Result(property = "taskid", column = "taskid"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "executed", column = "executed"),
        @Result(property = "currenttask", column = "currenttask")
	})
    public List<QueuePoint> getQueue(@Param("roboid") int roboid);
    
    @Select("SELECT * FROM queue "
        + " WHERE roboid < 0 AND executed = 0 "
        + " ORDER BY id ")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "roboid", column = "roboid"),
        @Result(property = "taskid", column = "taskid"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "executed", column = "executed"),
        @Result(property = "currenttask", column = "currenttask")
	})
    public List<QueuePoint> getAllUnsetQueuePonit();
    
    
    @Update("UPDATE queue SET duration=duration-1 WHERE executed=0 AND currenttask=1")
    @Options(flushCache = true,useCache=false)
    public int updateAllQueue();
    
    @Select("SELECT * FROM queue "
        + " WHERE duration < 1 AND executed = 0 AND currenttask = 1"
        + " ORDER BY id ")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "roboid", column = "roboid"),
        @Result(property = "taskid", column = "taskid"),
        @Result(property = "duration", column = "duration"),
        @Result(property = "executed", column = "executed"),
        @Result(property = "currenttask", column = "currenttask")
	})
    public List<QueuePoint> getEndedQueuePonit();
    
    // **************************** noties           ************************ //
    
    @Insert("INSERT INTO logs (name, activity)"
        + " VALUES ("
	+ "#{name},"
	+ "#{activity})")
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache=true) 
    public int addNewNote(Note note);
    
    @Update("UPDATE logs SET "
        + "name = #{name}, "
        + "activity = #{activity} "
        + "WHERE id = #{id}")
    @Options(flushCache = true,useCache=false)
    public int updateNote(Note note);
    
    @Select("SELECT * FROM logs "
        + " WHERE logs.id = #{id}")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "activity", column = "activity")
	})
    public Machine getNoteById(@Param("id") int id);
    
    @Select("SELECT * FROM logs")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "activity", column = "activity")
	})
    public List<Note> getAllNote();
    
    @Select("SELECT * FROM logs "
        + " WHERE logs.name LIKE #{name}")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "activity", column = "activity")
	})
    public List<Note> getNoteByName(@Param("name") String name);
    
    // **************************** machine          ************************ //
    
    @Insert("INSERT INTO robots (currentTask,alive,type)"
        + " VALUES ("
	+ "#{currentTask},"
	+ "#{alive},"
	+ "#{type})")
		 
    @Options(useGeneratedKeys = true, keyProperty = "id", flushCache=true) 
    public int addNewMachine(Machine machine);
    
    @Update("UPDATE robots SET "
        + "currentTask = #{currentTask}, "
        + "alive = #{alive}, "
        + "type = #{type} "
        + "WHERE id = #{id}")
    @Options(flushCache = true,useCache=false)
    public int updateMachine(Machine machine);
    
    @Select("SELECT * FROM robots "
        + " WHERE robots.id = #{id}")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "currentTask", column = "currentTask"),
        @Result(property = "alive", column = "alive"),
        @Result(property = "type", column = "type")
	})
    public Machine getMachineById(@Param("id") int id);
    
    @Select("SELECT * FROM robots")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "currentTask", column = "currentTask"),
        @Result(property = "alive", column = "alive"),
        @Result(property = "type", column = "type")
	})
    public List<Machine> getAllMachine();
    
    @Select("SELECT * FROM robots "
        + " WHERE robots.alive = 1")
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "currentTask", column = "currentTask"),
        @Result(property = "alive", column = "alive"),
        @Result(property = "type", column = "type")
	})
    public List<Machine> getAllAlivesMachine();
    
    @Select("SELECT myTable.id, "
	+ " myRobots.currentTask, "
	+ " myTable.alive, "
	+ " myTable.type "
        + " FROM (SELECT  "
            + " robots.id, "
            + " robots.alive, "
            + " robots.type, "
            + " SUM(queue.duration) as durationSum "
            + " FROM robots AS robots "
            + " left join queue AS queue  "
                + " ON robots.id = queue.roboid "
                + " AND queue.executed = 0 "
            + " WHERE robots.alive = 1 "
            + "group by "
            + "	robots.id, "
            + "	robots.alive, "
            +"  robots.type) AS myTable "
            +" left join robots as myRobots "
            +" ON myTable.id = myRobots.id "
        + " WHERE myTable.durationSum <= 0  "
        + " OR myTable.durationSum IS NULL ")
    
    @Options(flushCache=true,useCache=false)
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "currentTask", column = "currentTask"),
        @Result(property = "alive", column = "alive"),
        @Result(property = "type", column = "type")
	})
    public List<Machine> getAllFreeMashine();
    
}
