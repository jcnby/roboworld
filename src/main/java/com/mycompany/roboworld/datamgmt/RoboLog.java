package com.mycompany.roboworld.datamgmt;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.Note;

public class RoboLog extends DataService{
    
    public int save(Machine machine){
    	int result;
        result = saveInBase(machine.getId() > 0 ? getMapper().updateMachine(machine) : getMapper().addNewMachine(machine));
        return result;
    }
    
    public int saveActivity(Machine machine, String activity){
    	int result;
        result = saveInBase(getMapper().addNewNote(new Note("R"+machine.getId()+"T"+machine.getType(),activity)));
        return result;
    }
    
    private int saveInBase(int result){
        if(result!=0){
            commit();
        }
        closeSqlSession();
        return result;
    }
}
