package com.mycompany.roboworld.datamgmt;

import java.util.Properties;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class DataService {
    
    private SqlSessionFactory sqlSessionFactory;
    private SqlSession sqlSession;
    
    public DataService(){
    	sqlSessionFactory = MyBatisConnectionFactory.getInstance().getSqlSessionFactory();
    	sqlSession = null;
    }
    
    public SqlSession getSqlSession(){
    	if(sqlSession == null){
    		sqlSession = sqlSessionFactory.openSession();
    	}
    	return sqlSession;
    }
    
    public void commit(){ 
    	getSqlSession().commit(); 
    }

    public void closeSqlSession(){
    	try {
    		sqlSession.close();
    	}catch(Exception e){
    		// the session was not closed correctly
    	}finally{
    		sqlSession = null;
    	}
    }
    
    public Mapper getMapper(){
    	return getSqlSession().getMapper(Mapper.class); 
    }   
    
    public Properties getDBConfig(){
    	return MyBatisConnectionFactory.getInstance().getDBConfig();
    }
    
    public boolean isEmpty(String str){
    	if(str == null){
    		return true;
    	}
    	if(str.trim().isEmpty()){
    		return true;
    	}
    	return false;
    }
    
}
