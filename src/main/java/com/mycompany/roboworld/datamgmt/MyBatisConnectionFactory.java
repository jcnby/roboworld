package com.mycompany.roboworld.datamgmt;

import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.mycompany.roboworld.datamgmt.MyBatisConnectionFactory;
import com.mycompany.roboworld.datamgmt.MyBatisConnectionFactory.SingletonHolder;

public class MyBatisConnectionFactory {
    
    public static final String DB_CONFIG_FILE_PATH = "config.properties";	
    
    private SqlSessionFactory sqlSessionFactory;
    private Reader mybatis_config_reader; 
    private Properties config_props = null;
	
    // Realization by Bill Pugh: http://programador.ru/singleton/
    public static class SingletonHolder {
        public static final MyBatisConnectionFactory INSTANCE = new MyBatisConnectionFactory();
    }       
    
    public static MyBatisConnectionFactory getInstance() { 
    	return SingletonHolder.INSTANCE;
    }
    
    public Properties getDBConfig(){
    	if(config_props != null){
    		return config_props;
    	}
		try {
			config_props = Resources.getResourceAsProperties(DB_CONFIG_FILE_PATH);
		} catch (IOException e){
			System.out.println("ERROR: "+e.getMessage());
		}
	    return config_props;
    }
    
    public synchronized SqlSessionFactory getSqlSessionFactory() { 
    	try {
    		if(mybatis_config_reader == null){
            	mybatis_config_reader = Resources.getResourceAsReader(getDBConfig().getProperty("mybatis.config"));
            }
            if (sqlSessionFactory == null){
            	sqlSessionFactory = new SqlSessionFactoryBuilder().build(mybatis_config_reader);
            }
        }catch(Exception e){
        	System.out.println("ERROR: "+e.getMessage());
		}
        return sqlSessionFactory;
    }
}
