package com.mycompany.roboworld.util;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.MilitaryRobot;

public class MilitaryMaker implements RobotMaker{

    public Robot createRobot() {
        return new MilitaryRobot();
    }
    
    public Robot createRobot(Machine machine) {
        return new MilitaryRobot(machine);
    }
    
}
