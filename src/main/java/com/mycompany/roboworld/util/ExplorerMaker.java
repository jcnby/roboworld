package com.mycompany.roboworld.util;

import com.mycompany.roboworld.entities.ExplorerRobot;
import com.mycompany.roboworld.entities.Machine;

public class ExplorerMaker implements RobotMaker{

    public Robot createRobot() {
        return new ExplorerRobot();
    }
    
    public Robot createRobot(Machine machine) {
        return new ExplorerRobot(machine);
    }
    
}
