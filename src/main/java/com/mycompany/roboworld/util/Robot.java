package com.mycompany.roboworld.util;

import com.mycompany.roboworld.entities.Machine;

public interface Robot {
    
    Machine getMachine();
    
    void setTask(String task);
    
}

