package com.mycompany.roboworld.util;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.entities.MinerRobot;

public class MinerMaker implements RobotMaker{

    public Robot createRobot() {
        return new MinerRobot();
    }
    
    public Robot createRobot(Machine machine) {
        return new MinerRobot(machine);
    }
    
}
