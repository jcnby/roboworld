package com.mycompany.roboworld.util;

import com.mycompany.roboworld.entities.Machine;

public interface RobotMaker {
	
    Robot createRobot();
    
    Robot createRobot(Machine machine);
}
