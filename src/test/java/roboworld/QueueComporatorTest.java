package roboworld;

import com.mycompany.roboworld.entities.Machine;
import com.mycompany.roboworld.services.BaseService;
import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import org.junit.Test;
import static org.junit.Assert.*;

public class QueueComporatorTest {

	public QueueComporatorTest() {
    }

    @Test
    public void testQueueComporatorTest() {
        int typeMachine = 1;
        List<Machine> freeMashineList = new ArrayList<Machine>();
        freeMashineList.add(new Machine(6, "empty", true, 1));
        freeMashineList.add(new Machine(5, "empty", true, 1));
        freeMashineList.add(new Machine(2, "empty", true, 2));
        freeMashineList.add(new Machine(3, "empty", true, 1));
        Queue<Machine> freeMashineQueue = new PriorityQueue<Machine>(freeMashineList.size(), BaseService.getComparator(typeMachine));    
        freeMashineQueue.addAll(freeMashineList);
        Machine robo = freeMashineQueue.peek();
        assertTrue("Comparator didn't work", robo.getId()==3);
    }

}
