package roboworld;

import com.mycompany.roboworld.datamgmt.DataService;
import com.mycompany.roboworld.entities.Task;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class DataBasePrepared  extends DataService{

	public DataBasePrepared() {
    }

    @Test
    public void testDataBasePrepared(){
        List<Task> taskList = new ArrayList<Task>();
        try{
            taskList = getMapper().getTasks();
        }finally{
            closeSqlSession();
        }
        Integer[] tasks = new Integer[4];
        tasks[0] = 0;
        tasks[1] = 3;
        tasks[2] = 4;
        tasks[3] = 7;
        
        assertTrue("You need fill task in database!",taskList.size()==4);
        for(int i = 0; i < 4 ; i++){
            assertTrue("Error. task id : " + (i + 1) + " must have type: " + i + " and duration: " + tasks[i], getResult(i,tasks[i],taskList));
        }
        
    }
    
    private boolean getResult(int type,int duration,List<Task> taskList){
        boolean  result = false;
        for(Task currentTask : taskList){
            if(currentTask.getId()==type+1){
                result = currentTask.getType() == type && currentTask.getDuration() == duration ? true : false;
                break;
            }
        }
        return result;
    }

}
