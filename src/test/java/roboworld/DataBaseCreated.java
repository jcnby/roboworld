package roboworld;


import com.mycompany.roboworld.datamgmt.DataService;
import org.junit.Test;
import static org.junit.Assert.*;

public class DataBaseCreated extends DataService{

	public DataBaseCreated() {
    }

    @Test
    public void testDataBase(){
    
        boolean result = true;
        
        try{
            getMapper().getAllMachine();
            getMapper().getAllNote();
            getMapper().getTasks();
            getMapper().getEndedQueuePonit();
        }catch (Exception e){
            result = false;
        }finally{
            closeSqlSession();
        }
        assertTrue("DataBase does not exist. You need initialize database", result);
    }

}
